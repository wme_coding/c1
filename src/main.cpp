#include <iostream>
#include <cassert>
#include "../include/vector.h"
#include <cassert>

void testShityVector(){
    Vector shityVector1;
    Vector shityVector2(30);
    Vector shityVector3(30, 5);
    Vector shityVector4(shityVector3);
    try {
        Vector shityVector(-3);
    } catch(std::bad_array_new_length){
        std::cout << "Bad length" << std::endl;
    }
}

void testGetElem(){
    Vector shityVector(10, 4);
    assert(shityVector[5] == 4);
    try {
        shityVector[70];
    } catch(std::logic_error){
        std::cout << "Wrong Index" << std::endl;
    }
}

void testNewSize(){
    Vector shityVector(3, 4);
    shityVector.newSize(5);
    std::cout << shityVector;
}

void testCopy(){
    Vector shityVector1(3, 4);
    Vector shityVector2(6, 6);
    std::cout << shityVector1 << std::endl;
    std::cout << shityVector2 << std::endl;
    shityVector1 = shityVector2;
    shityVector1[3] = 2;
    std::cout << shityVector1 << std::endl;
    std::cout << shityVector2 << std::endl;
}

void testEquality(){
    Vector shityVector1(3, 4);
    Vector shityVector2(4, 5);
    try {
        bool e = shityVector1 == shityVector2;
    } catch (std::logic_error){
        std::cout << "Lengths are not the same" << std::endl;
    }
    shityVector1 = shityVector2;
    bool e = shityVector1 == shityVector2;
    std::cout << e << std::endl;
}

void testConcatination(){
    Vector shityVector1(3, 5);
    Vector shityVector2(6, 9);
    shityVector1 + shityVector2;
    std::cout << shityVector1 << std::endl;
}
void testComparicon(){
    Vector shityVector1(5, 0);
    Vector shityVector2(4, 1);
    std::cout << (shityVector2 < shityVector1) << std::endl;
    std::cout << (shityVector2 > shityVector1) << std::endl;
    shityVector1 = shityVector2;
    std::cout << (shityVector2 <= shityVector1) << std::endl;
    std::cout << (shityVector2 >= shityVector1) << std::endl;
}

void testReserve(){
    Vector shityVector(5, 0);
    shityVector.reserve(6);
    for(int i = 0; i < 5; i ++){
        shityVector[i] = i;
    }
    std::cout << shityVector << std::endl;
    std::cout << shityVector.getCapacity() << std::endl;
    shityVector.newSize(2);
    std::cout << shityVector << std::endl;
    std::cout << shityVector.getCapacity() << std::endl;
    shityVector.newSize(8);
    std::cout << shityVector << std::endl;
    std::cout << shityVector.getCapacity() << std::endl;
}

void testPushPop(){
    Vector shityVector(5, 0, 6);
    for(int i = 0; i < 5; i ++){
        shityVector[i] = i;
    }
    std::cout << shityVector << std::endl;
    std::cout << shityVector.getCapacity() << std::endl;
    shityVector.pushBack(888);
    std::cout << shityVector.getCapacity() << std::endl;
    std::cout << shityVector.popBack() << std::endl;
    std::cout << shityVector.getCapacity() << std::endl;
}

int main() {
    //testShityVector();
    //testGetElem();
    //testNewSize();
    //testCopy();
    //testEquality();
    //testConcatination();
    //testComparicon();
    //testReserve();
    testPushPop();
    return 0;
}
