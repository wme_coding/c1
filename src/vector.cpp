#include "../include/vector.h"

Vector::Vector() {
    this->vector = new int[DEFAULT_SIZE];
    this->size = DEFAULT_SIZE;
    this->capacity = 0;
    for(int i = 0; i < size; i++){
        vector[i] = 0;
    }
}

Vector::Vector(int size) {
    this->vector = new int[size];
    this->size = size;
    this->capacity = 0;
    for(int i = 0; i < size; i++){
        vector[i] = 0;
    }
}

Vector::Vector(int size, int initialNumber) {
    this->vector = new int[size];
    this->size = size;
    this->capacity = 0;
    for(int i = 0; i < size; i++){
        vector[i] = initialNumber;
    }
}

Vector::Vector(int size, int initialNumber, int capacity) {
    this->vector = new int[size];
    this->size = size;
    this->capacity = 0;
    for(int i = 0; i < size; i++){
        vector[i] = initialNumber;
    }
    this->capacity = capacity;
}

Vector::Vector(const Vector &other) {
    this->size = other.size;
    this->vector = new int[this->size];
    this->capacity = other.capacity;
    for(int i = 0; i < this->size; i++){
        this->vector[i] = other.vector[i];
    }
}

Vector::~Vector() {
    delete[] vector;
}

int Vector::getSize() const {
    return size;
}

int &Vector::operator[](int i) {
    if(i >= size || i < 0){
        throw std::logic_error("Wrong index");
    }
    return vector[i];
}

void Vector::newSize(int size) {
    int *temp = nullptr;
    if(size > this->size + capacity){
        temp = new int [size];
        for(int i = 0; i < this->size; i++){
            temp[i] = vector[i];
        }
        for(int i = this->size; i < size; i++){
            temp[i] = 0;
        }
        capacity = 0;
        delete [] vector;
        vector = temp;
    }
    else if(size > this->size && size <= this->size + capacity){
        this->capacity = this->size + this->capacity - size;
    }
    else if(size < this->size){
        this->capacity = this->capacity + this->size - size;
    }
    else{
        return;
    }
    this->size = size;
}

void Vector::reserve(int n) {
    int *temp = new int [this->size + n];
    for(int i = 0; i < this->size; i++){
        temp[i] = vector[i];
    }
    for(int i = this->size; i < this->size + n; i++){
        temp[i] = 0;
    }
    this->capacity = n;
    delete [] vector;
    vector = temp;
}

int Vector::getCapacity() {
    return this->capacity;
}

Vector &Vector::operator=(const Vector &other) {
    if (&other == this)
        return *this;
    this->size = other.size;
    newSize(this->size);
    for(int i = 0; i < this->size; i++){
        vector[i] = other.vector[i];
    }
    return *this;
}

bool Vector::operator==(const Vector &other) const {
    if(other.size != this->size){
        throw std::logic_error("Different sizes");
    }
    for(int i = 0; i < this->size; i++){
        if(this->vector[i] != other.vector[i]){
            return false;
        }
    }
    return true;
}

bool Vector::operator!=(const Vector &other) const {
    return !(other == *this);
}

    bool Vector::operator<(const Vector &other) const {
    if(this->size < other.size){
        return true;
    } else if(this->size > other.size){
        return false;
    }
    for(int i = 0; i < this->size; i++){
        if(this->vector[i] >= other.vector[i]){
            return false;
        }
    }
    return true;
}

bool Vector::operator>(const Vector &other) const {
    return !(*this < other);
}

bool Vector::operator<=(const Vector &other) const {
    if(this->size < other.size){
        return true;
    } else if(this->size > other.size){
        return false;
    }
    for(int i = 0; i < this->size; i++){
        if(this->vector[i] > other.vector[i]){
            return false;
        }
    }
    return true;
}

bool Vector::operator>=(const Vector &other) const {
    if(this->size > other.size){
        return true;
    } else if(this->size < other.size){
        return false;
    }
    for(int i = 0; i < this->size; i++){
        if(this->vector[i] < other.vector[i]){
            return false;
        }
    }
    return true;
}

void Vector::operator+(const Vector &other) {
    int temp = this->size;
    newSize(this->size + other.size);
    for(int i = temp; i < this->size; i++){
        this->vector[i] = other.vector[i - temp];//железная хватка
    }
}

std::ostream &operator<<(std::ostream &os, const Vector &shityVector) {
    os << "size:" <<shityVector.size << "\n";
    for(int i = 0; i < shityVector.size; i++){
        os << shityVector.vector[i] << " ";
    }
    return os;
}

std::istream &operator>>(std::istream &is, const Vector &shityVector) {
    is >> shityVector.size;
    for(int i = 0; i < shityVector.size; i++){
        is >> shityVector.vector[i];
    }
    return is;
}

Vector::Vector(Vector &&other): vector(other.vector), size(other.size), capacity(other.capacity) {
    other.vector = nullptr;
}

Vector &Vector::operator=(Vector &&other) {
    if (&other == this) {
        return *this;
    }
    delete [] vector;
    this->vector = other.vector;
    other.vector = nullptr;
    return *this;
}

void Vector::pushBack(int x) {
    if(capacity > 0){
        vector[size] = x;
        size++;
        capacity--;
    } else {
        reserve(10);
        pushBack(x);
    }
}

int Vector::popBack() {
    if(size == 0){
        throw std::logic_error("No elems");
    }
    int temp = vector[size - 1];
    vector[size - 1] = 0;
    capacity++;
    size--;
    return temp;
}




