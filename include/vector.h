#pragma once

#include <stdexcept>
#include <ostream>

const int DEFAULT_SIZE = 20;

class Vector{
    int *vector;
    int size;
    int capacity;

public:
    Vector();

    Vector(int size);

    Vector(int size, int initialNumber);

    Vector(int size, int initialNumber, int capacity);

    Vector(const Vector& other);

    Vector(Vector&& other);

    int getSize() const;

    int &operator[] (int i);

    void newSize(int size);

    void reserve(int n);

    int getCapacity();

    void pushBack(int x);

    int popBack();

   Vector& operator= (const Vector &other);

   Vector& operator= (Vector &&other);

    bool operator==(const Vector &other) const;

    bool operator!=(const Vector &other) const;

    bool operator<(const Vector &other) const;

    bool operator>(const Vector &other) const;

    bool operator<=(const Vector &other) const;

    bool operator>=(const Vector &other) const;

    void operator+(const Vector &other);

    friend std::ostream &operator<<(std::ostream &os, const Vector &shityVector);

    friend std::istream &operator>>(std::istream &os, const Vector &shityVector);

    ~Vector();
};
