cmake_minimum_required(VERSION 3.15)
project(C1)

set(CMAKE_CXX_STANDARD 14)

add_executable(C1 src/main.cpp include/vector.h src/vector.cpp)